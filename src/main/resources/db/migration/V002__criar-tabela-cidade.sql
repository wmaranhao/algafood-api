create table cidade (
	id bigint not null auto_increment,
	nome_cidade varchar(80) not null,
    nome_estado varchar(80) not null,
    
    primary key(id)
) engine=InnoDB default charset=utf8;

insert Into cidade (id, nome_cidade, nome_estado) values
(1, "Uberlândia", "Minas Gerais"),
(2, "Belo Horizonte", "Minas Gerais"),
(3, "São Paulo", "São Paulo"),
(4, "Recife", "Pernambuco");