package com.dawis.algafood.domain.repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.dawis.algafood.domain.model.Restaurante;

public interface RestauranteRepository extends CustomJpaRepository<Restaurante, Long>, RestauranteRepositoryQueries, JpaSpecificationExecutor<Restaurante> {

	@Query("from Restaurante r join fetch r.cozinha left join fetch r.formasPagamento")
	List<Restaurante> findAll();
	
	//Spring JPA interpreta o nome do método com palavras chaves e cria a implementação ....
	List<Restaurante> queryByTaxaFreteBetween(BigDecimal taxaInicial, BigDecimal taxaFinal);
	
	//Spring JPA interpreta o nome do método com palavras chaves e cria a implementação ....
	//List<Restaurante> findByNomeContainingAndCozinhaId(String nome, Long cozinha);
	
	/* Usando a annotation Query para criar o sql e deixando o nome do método mais 
	 * simples de ser lido e a possibilidade de criar querys mais complexas,
	 * substituindo o método de cima findByNomeContainingAndCozinhaId.... */
	//@Query("from Restaurante where nome like %:nome% and cozinha.id = :id")
	//List<Restaurante> consultarPorNome(String nome, @Param("id") Long cozinha);
	
	//Esta usando um arquivo orm.xml dentro de META-INF para query nomeada ...
	List<Restaurante> consultarPorNome(String nome, @Param("id") Long cozinha);
	
	//Spring JPA interpreta o nome do método com palavras chaves e cria a implementação ....
	Optional<Restaurante> findFirstRestauranteByNomeContaining(String nome);
	
	//Spring JPA interpreta o nome do método com palavras chaves e cria a implementação ....
	List<Restaurante> findTop2ByNomeContaining(String nome);
	
	//Spring JPA interpreta o nome do método com palavras chaves e cria a implementação ....
	int countByCozinhaId(Long cozinha);
	
	/*
	 * Esse método vai chamar a classe(RestauranteRepositoryImpl) que implementa esse código customizado ....
	 * Essa claase e chamada pelo spring por conta da da classe que implementa ter uma extrutura padão reconhecida pelo spring
	 * nome da interface "RestauranteRepository" + sufixo "Impl"
	 *  
	 **/
	//List<Restaurante> find(String nome, BigDecimal taxaFreteInicial, BigDecimal taxaFreteFinal);
}
