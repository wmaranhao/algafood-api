package com.dawis.algafood.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dawis.algafood.domain.model.FormaPagamento;

public interface FormaPagamentoRepository extends JpaRepository<FormaPagamento, Long> {

}
