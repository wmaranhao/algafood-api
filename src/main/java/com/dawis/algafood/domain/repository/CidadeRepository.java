package com.dawis.algafood.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dawis.algafood.domain.model.Cidade;

public interface CidadeRepository extends JpaRepository<Cidade, Long> {

}
