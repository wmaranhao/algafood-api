package com.dawis.algafood.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dawis.algafood.domain.model.Permissao;

public interface PermissaoRepository extends JpaRepository<Permissao, Long> {

}
