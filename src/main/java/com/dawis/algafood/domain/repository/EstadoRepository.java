package com.dawis.algafood.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dawis.algafood.domain.model.Estado;

public interface EstadoRepository extends JpaRepository<Estado, Long> {

}
