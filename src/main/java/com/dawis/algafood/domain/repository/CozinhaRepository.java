package com.dawis.algafood.domain.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.dawis.algafood.domain.model.Cozinha;

@Repository
public interface CozinhaRepository extends CustomJpaRepository<Cozinha, Long> {

	//Coloca o nome do métoso igual de uma propriedade do objeto(Cozinha) o qual você gostaria de pesquisar ...
	//Spring JPA interpreta o nome do método com palavras chaves e cria a implementação ....
	List<Cozinha> nome(String nome);
	
	//Ou
	//FindBy como préfixo e o nome da propriedade ...
	//Spring JPA interpreta o nome do método com palavras chaves e cria a implementação ....
	List<Cozinha> findByNome(String nome);
	
	//Spring JPA interpreta o nome do método com palavras chaves e cria a implementação ....
	boolean existsByNome(String nome);
}
