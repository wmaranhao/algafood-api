package com.dawis.algafood.domain.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.dawis.algafood.domain.exception.EntidadeEmUsoException;
import com.dawis.algafood.domain.exception.EntidadeNaoEncontradaException;
import com.dawis.algafood.domain.model.Cozinha;
import com.dawis.algafood.domain.model.Restaurante;
import com.dawis.algafood.domain.repository.CozinhaRepository;
import com.dawis.algafood.domain.repository.RestauranteRepository;

@Service
public class CadastroRestauranteService {

	@Autowired
	private RestauranteRepository restauranteRepository;
	
	@Autowired
	private CozinhaRepository cozinhaRepository;
	
	public Restaurante salvar(Restaurante restaurante) {
		Long cozinhaId = restaurante.getCozinha().getId();
		Cozinha cozinha = cozinhaRepository.findById(cozinhaId).orElseThrow(() -> new EntidadeNaoEncontradaException(
				String.format("Não existe cadastro de cozinha com código %d", cozinhaId)));
		
		restaurante.setCozinha(cozinha);
		
		return restauranteRepository.save(restaurante);
	}
	
	public List<Restaurante> listar() {
		return restauranteRepository.findAll();
	}
	
	public Optional<Restaurante> buscar(Long restauranteId) {
		return restauranteRepository.findById(restauranteId);
	}
	
	public void excluir(Long restauranteId) {
		try {
			restauranteRepository.deleteById(restauranteId);
		} 
		catch (EmptyResultDataAccessException e) {
			throw new EntidadeNaoEncontradaException(String.format("Não existe um cadastro de restaurante com código %d", restauranteId));
		} 
		catch (DataIntegrityViolationException e) {
			throw new EntidadeEmUsoException(String.format("Restaurante de código %d não pode ser removida, pois está em uso", restauranteId));
		}
	}
}
