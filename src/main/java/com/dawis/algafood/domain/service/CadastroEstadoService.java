package com.dawis.algafood.domain.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.dawis.algafood.domain.exception.EntidadeEmUsoException;
import com.dawis.algafood.domain.exception.EntidadeNaoEncontradaException;
import com.dawis.algafood.domain.model.Estado;
import com.dawis.algafood.domain.repository.EstadoRepository;

@Service
public class CadastroEstadoService {

	@Autowired
	private EstadoRepository estadoRepository;
	
	public Estado salvar(Estado estado) {
		return estadoRepository.save(estado);
	}

	public List<Estado> listar() {
		return estadoRepository.findAll();
	}
	
	public Optional<Estado> buscar(Long estadoId) {
		return estadoRepository.findById(estadoId);
	}
	
	public void excluir(Long estadoId) {
		try {
			estadoRepository.deleteById(estadoId);
		} 
		catch (EmptyResultDataAccessException e) {
			throw new EntidadeNaoEncontradaException(String.format("Não existe um cadastro de estado com código %d", estadoId));
		} 
		catch (DataIntegrityViolationException e) {
			throw new EntidadeEmUsoException(String.format("Estado de código %d não pode ser removida, pois está em uso", estadoId));
		}
	}
}
