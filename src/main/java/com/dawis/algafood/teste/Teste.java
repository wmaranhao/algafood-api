package com.dawis.algafood.teste;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

public class Teste {

	public static void main(String[] args){
		LocalTime time = LocalTime.of(00, 00, 15);
		System.out.println(String.format("%sh%sm%ss", time.getHour(), time.getMinute(), time.getSecond()));
		
		String chro = String.format("%s", time.truncatedTo(ChronoUnit.SECONDS));
		System.out.println(chro);
		
		LocalDateTime time2 = LocalDateTime.now();
		System.out.println(String.format("%sh%sm%ss", time2.getHour(), time2.getMinute(), time2.getSecond()));
	}
}
